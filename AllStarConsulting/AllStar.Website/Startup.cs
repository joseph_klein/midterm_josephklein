﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AllStar.Website.Startup))]
namespace AllStar.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
