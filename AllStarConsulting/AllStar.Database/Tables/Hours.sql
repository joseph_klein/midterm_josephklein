﻿CREATE TABLE [dbo].[Hours]
(
	[HourId] INT NOT NULL IDENTITY(1,1),
	[JobId] INT NOT NULL,
	[TaskId] INT NOT NULL,
	[Date] DATE NOT NULL,
	[Hours] DECIMAL(4,2) NOT NULL,
	
	PRIMARY KEY ([JobId], [TaskId], [HourId]),
	FOREIGN KEY ([JobId]) REFERENCES [dbo].[Jobs] ([JobId]) ON DELETE CASCADE,
	FOREIGN KEY ([TaskId]) REFERENCES [dbo].[JobTasks] ([TaskId]) ON DELETE CASCADE,
)
