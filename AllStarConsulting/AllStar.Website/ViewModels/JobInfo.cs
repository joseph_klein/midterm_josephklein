﻿using AllStar.Website.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AllStar.Website.ViewModels
{
    public class JobInfo
    {
        public int JobId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(20, ErrorMessage = "Must be less than 20 characters")]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        [Required(ErrorMessage = "Description is required")]
        [StringLength(500, ErrorMessage = "Must be less than 500 characters")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Hourly wage is required")]
        [Range(0, 100, ErrorMessage = "Hourly wage must be less than 100")]
        [Display(Name = "Hourly Wage")]
        public decimal HourlyWage { get; set; }

        [Required(ErrorMessage = "Start date is required")]
        [DataType(DataType.Date, ErrorMessage = "Invalid Date.")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "date")]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Invalid Date.")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "date")]
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Completion")]
        public bool CompletedFlag { get; set; }

        [StringLength(50, ErrorMessage = "Must be less than 50 characters")]
        public string EmployeeName { get; set; }
        public string UserId { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hour> Hours { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobTask> Tasks { get; set; }


        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}