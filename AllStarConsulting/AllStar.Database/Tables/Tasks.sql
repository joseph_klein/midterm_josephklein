﻿CREATE TABLE [dbo].[JobTasks]
(
	[TaskId] INT NOT NULL IDENTITY(1, 1),
	[Name] NVARCHAR(20) NOT NULL,
	[Description] TEXT NOT NULL,
	[EstimatedHours] INT NOT NULL,
	[CompletedFlag] BIT NOT NULL,
	[JobId] INT NOT NULL,

	PRIMARY KEY ([TaskId]),
	FOREIGN KEY ([JobId]) REFERENCES [dbo].[Jobs] ([JobId]),
)
