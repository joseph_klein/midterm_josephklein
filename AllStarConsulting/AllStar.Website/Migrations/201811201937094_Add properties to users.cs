namespace AllStar.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addpropertiestousers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String(nullable: false, maxLength: 40));
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String(nullable: false, maxLength: 40));
            AddColumn("dbo.AspNetUsers", "Title", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.AspNetUsers", "Address1", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.AspNetUsers", "Address2", c => c.String(maxLength: 15));
            AddColumn("dbo.AspNetUsers", "City", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.AspNetUsers", "State", c => c.String(nullable: false, maxLength: 2));
            AddColumn("dbo.AspNetUsers", "Zip", c => c.String(nullable: false, maxLength: 15));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Zip");
            DropColumn("dbo.AspNetUsers", "State");
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "Address2");
            DropColumn("dbo.AspNetUsers", "Address1");
            DropColumn("dbo.AspNetUsers", "Title");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "FirstName");
        }
    }
}
