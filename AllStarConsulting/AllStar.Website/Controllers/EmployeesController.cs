﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AllStar.Website.Models;
using AllStar.Website.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NLog;
using PagedList;

namespace AllStar.Website.Controllers
{
    public class EmployeesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Employees
        [Authorize(Roles = "HR")]
        public ActionResult Index(
            string search,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            IEnumerable<ApplicationUser> emps = db.Users.ToList();


            if (!String.IsNullOrWhiteSpace(search))
            {
                emps = emps.Where(
                       m => m.FirstName.Contains(search) ||
                       m.LastName.Contains(search) ||
                       m.Title.Contains(search) ||
                       m.EmployeeNumber.ToString().Contains(search)
                    );
            }

            switch (sortBy)
            {
                default:
                case "first":
                    emps = emps.OrderBy(m => m.FirstName);
                    break;
                case "last":
                    emps = emps.OrderBy(m => m.LastName);
                    break;
                case "empTitle":
                    emps = emps.OrderBy(m => m.Title);
                    break;
            }

            var model = new EmployeeSearchResults
            {
                Search = search,
                SortBy = sortBy,
                Users = emps.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            return View(model);
        }

       

        // GET: Employees/Details/5
        [Authorize]
        public ActionResult Details(string id)
        {
            try
            { 
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ApplicationUser applicationUser = db.Users.Find(id);
                if (applicationUser == null)
                {
                    return HttpNotFound();
                }

               
                logger.Info($"{User.Identity.Name} viewed {applicationUser.FirstName} {applicationUser.LastName}");
                return View(applicationUser);
              
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        public ActionResult GetUserDetails()
        {
            return RedirectToAction("Details", "Employees", new { id = User.Identity.GetUserId() });
        }

        public ActionResult GetUserJob()
        {
            return RedirectToAction("Index", "Jobs", new { id = User.Identity.GetUserId() });
        }

        // GET: Employees/Create
        [Authorize(Roles = "HR")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "HR")]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Title,Address1,Address2,City,State,Zip,EmployeeNumber,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            try
            { 
            if (ModelState.IsValid)
            {
                db.Users.Add(applicationUser);
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} created {applicationUser.FirstName} {applicationUser.LastName}");
                    return RedirectToAction("Index");
            }

            return View(applicationUser);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            UserEdit model = new UserEdit()
            {
                Id = applicationUser.Id,
                Email = applicationUser.Email,
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName,
                Title = applicationUser.Title,
                Address1 = applicationUser.Address1,
                Address2 = applicationUser.Address2,
                City = applicationUser.City,
                State = applicationUser.State,
                Zip = applicationUser.Zip,
                EmployeeNumber = applicationUser.EmployeeNumber,
                PhoneNumber = applicationUser.PhoneNumber,
            };

            ViewBag.Roles = db.Roles.ToList();

            return View(model);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async System.Threading.Tasks.Task<ActionResult> Edit(
            //[Bind(Include = "Id,FirstName,LastName,Title,Address1,Address2,City,State,Zip,EmployeeNumber,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")]
        UserEdit applicationUser)
        {
            try
            { 
            if (ModelState.IsValid)
            {
                var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
                var manager = new UserManager<ApplicationUser>(store);
                var currentUser = manager.FindByEmail(applicationUser.Email);
                if (!String.IsNullOrWhiteSpace(applicationUser.Role))
                {
                        if(db.Roles.Find(applicationUser.Id) != null)
                        {
                            manager.RemoveFromRole(currentUser.Id, "Admin");
                            manager.RemoveFromRole(currentUser.Id, "Contractor");
                            manager.RemoveFromRole(currentUser.Id, "HR");
                            manager.RemoveFromRole(currentUser.Id, "Manager");
                        }

                       manager.AddToRole(currentUser.Id, applicationUser.Role);
                        
                }
                currentUser.FirstName = applicationUser.FirstName;
                currentUser.LastName = applicationUser.LastName;
                currentUser.Address1 = applicationUser.Address1;
                currentUser.Address2 = applicationUser.Address2;
                currentUser.State = applicationUser.State;
                currentUser.City = applicationUser.City;
                currentUser.Zip = applicationUser.Zip;
                currentUser.Title = applicationUser.Title;
                currentUser.UserName = applicationUser.Email;
                currentUser.Email = applicationUser.Email;
                currentUser.PhoneNumber = applicationUser.PhoneNumber;
                currentUser.EmployeeNumber = applicationUser.EmployeeNumber;
                await manager.UpdateAsync(currentUser);
                var ctx = store.Context;
                ctx.SaveChanges();
                    logger.Info($"{User.Identity.Name} edited {applicationUser.FirstName} {applicationUser.LastName}");
                    //db.Entry(applicationUser).State = EntityState.Modified;
                    //db.SaveChanges();
                    if(this.User.IsInRole("HR"))
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("Details", new { id = this.User.Identity.GetUserId() });
                    }
            }
            return View(applicationUser);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: Employees/Delete/5
        [Authorize(Roles = "HR")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "HR")]
        public async System.Threading.Tasks.Task<ActionResult> DeleteConfirmed(string id)
        {
            try
            { 
            ApplicationUser applicationUser = db.Users.Find(id);

            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(store);
            var currentUser = manager.FindByEmail(applicationUser.Email);

            await manager.DeleteAsync(currentUser);
            var ctx = store.Context;
            
            ctx.SaveChanges();
                logger.Info($"{User.Identity.Name} deleted {applicationUser.FirstName} {applicationUser.LastName}");

                //db.Users.Remove(applicationUser);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
