﻿using AllStar.Website.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AllStar.Website.ViewModels
{
    public class EmployeeSearchResults
    {
        [Display(Name = "Search")]
        public string Search { get; set; }
        [Display(Name = "Sort By")]
        public string SortBy { get; set; }

        public IPagedList<ApplicationUser> Users { get; set; }
    }
}