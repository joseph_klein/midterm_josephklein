﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AllStar.Website.Models;
using AllStar.Website.ViewModels;
using NLog;
using PagedList;

namespace AllStar.Website.Controllers
{
    public class JobsController : Controller
    {
        private AllStar_Database db = new AllStar_Database();
        private ApplicationDbContext usersDb = new ApplicationDbContext();
        private Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Jobs
        public ActionResult Index(
            string id,
            string search,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            IQueryable<Job> jobs = db.Jobs.OrderBy(m => m.CompletedFlag == true);


            if (!String.IsNullOrWhiteSpace(search))
            {
                jobs = jobs.Where(
                       m => m.JobId.ToString().Contains(search) ||
                       m.Name.Contains(search) ||
                       m.Description.Contains(search)
                    );
            }

            switch (sortBy)
            {
                default:
                case "jobName":
                    jobs = jobs.OrderBy(m => m.Name);
                    break;
                case "startDate":
                    jobs = jobs.OrderBy(m => m.StartDate);
                    break;
                case "progress":
                    jobs = jobs.Where(m => m.CompletedFlag == false);
                    break;
                case "complete":
                    jobs = jobs.Where(m => m.CompletedFlag == true);
                    break;
            }

            if (this.User.IsInRole("Contractor"))
            {
                jobs = jobs.Where(m => m.UserId == id);
            }

            var model = new JobSearchResult
            {
                ID = id,
                Search = search,
                SortBy = sortBy,
                Jobs = jobs.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };



            return View(model);
        }

        // GET: Jobs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            if(job.UserId != null)
            {
                ViewBag.EmpName = usersDb.Users.Find(job.UserId);
            }
            ApplicationUser curentUser = usersDb.Users.Find(job.UserId);
            if (curentUser != null)
            {
                ViewBag.EmpName = $"{curentUser.FirstName} {curentUser.LastName}";
            }
            logger.Info($"{User.Identity.Name} viewed {job.Name}");
            return View(job);
        }

        // GET: Jobs/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Jobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobId,Name,Description,HourlyWage,StartDate,EndDate,CompletedFlag,EmployeeName,UserId")] Job job)
        {
            try
            { 
            ApplicationUser employee = usersDb.Users.Find(job.UserId);

            if (ModelState.IsValid)
            {
               if(employee != null)
                {
                    job.UserId = employee.Id;
                    job.EmployeeName = $"{employee.FirstName} {employee.LastName}";
                }
                db.Jobs.Add(job);
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} created {job.Name}");
                    return RedirectToAction("Index");
            }
            
            return RedirectToAction("Details", new { id = job.JobId});
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: Jobs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }

            JobInfo model = new JobInfo()
            {
                JobId = job.JobId,
                Name = job.Name,
                Description = job.Description,
                HourlyWage = job.HourlyWage,
                StartDate = job.StartDate,
                EndDate = job.EndDate,
                CompletedFlag = job.CompletedFlag,
                EmployeeName = job.EmployeeName,
                UserId = job.UserId,
                Hours = job.Hours,
                Tasks = job.Tasks,
                Users = usersDb.Users.Where(m => m.EmployeeNumber > 0).ToList()
            };


            ApplicationUser curentUser = usersDb.Users.Find(job.UserId);
            if (curentUser != null)
            {
                ViewBag.EmpName = $"{curentUser.FirstName} {curentUser.LastName}";
            }

            return View(model);
        }

        // POST: Jobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobId,Name,Description,HourlyWage,StartDate,EndDate,CompletedFlag,EmployeeName,UserId")] JobInfo model)
        {
            try
            { 
            Job job = db.Jobs.Find(model.JobId);
                job.JobId = model.JobId;
                job.Name = model.Name;
                job.Description = model.Description;
                job.HourlyWage = model.HourlyWage;
                job.StartDate = model.StartDate;
                job.EndDate = model.EndDate;
                job.CompletedFlag = model.CompletedFlag;
                job.Hours = model.Hours;
                job.Tasks = model.Tasks;
            ApplicationUser employee = usersDb.Users.Find(model.UserId);

            if (ModelState.IsValid)
            {
                if (employee != null)
                {
                    job.UserId = employee.Id;
                    job.EmployeeName = $"{employee.FirstName} {employee.LastName}";
                }
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} edited {job.Name}");
                    return RedirectToAction("Index");
            }
            return View(job);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: Jobs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            ApplicationUser curentUser = usersDb.Users.Find(job.UserId);
            if (curentUser != null)
            {
                ViewBag.EmpName = $"{curentUser.FirstName} {curentUser.LastName}";
            }
            return View(job);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { 
            Job job = db.Jobs.Find(id);
            db.Jobs.Remove(job);
            db.SaveChanges();
                logger.Info($"{User.Identity.Name} deleted {job.Name}");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        public ActionResult Complete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Complete")]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteConfirmed(int id)
        {
            try
            { 
            Job job = db.Jobs.Find(id);
            bool isComplete = false;
            foreach(var task in job.Tasks)
            {
                if(task.CompletedFlag)
                {
                    isComplete = true;
                }
                else
                {
                    return HttpNotFound();
                }
            }

            if(isComplete && !job.CompletedFlag)
            {
                job.CompletedFlag = true;
                job.EndDate = DateTime.Today;
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} completed {job.Name}");
                    return RedirectToAction("GetUserJob", "Employees", null);
            }
            else
            {
                job.CompletedFlag = false;
                job.EndDate = null;
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} uncompleted {job.Name}");
                    return RedirectToAction("GetUserJob", "Employees", null);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
