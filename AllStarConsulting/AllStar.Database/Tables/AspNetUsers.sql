﻿CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [EmployeeNumber]       INT               NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    [FirstName]            NVARCHAR (40)  DEFAULT ('') NOT NULL,
    [LastName]             NVARCHAR (40)  DEFAULT ('') NOT NULL,
    [Title]                NVARCHAR (20)  DEFAULT ('') NOT NULL,
    [Address1]             NVARCHAR (30)  DEFAULT ('') NOT NULL,
    [Address2]             NVARCHAR (15)  NULL,
    [City]                 NVARCHAR (20)  DEFAULT ('') NOT NULL,
    [State]                NVARCHAR (2)   DEFAULT ('') NOT NULL,
    [Zip]                  NVARCHAR (15)  DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AspNetUsers]([UserName] ASC);

