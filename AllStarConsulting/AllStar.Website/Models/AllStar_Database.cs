namespace AllStar.Website.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AllStar_Database : DbContext
    {
        public AllStar_Database()
            : base("name=AllStar_Database")
        {
            Database.SetInitializer<AllStar_Database>(null);
        }
        
        public virtual DbSet<Hour> Hours { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobTask> Tasks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hour>()
                .Property(e => e.Hours)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Job>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.HourlyWage)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.Description)
                .IsUnicode(false);
        }
    }
}
