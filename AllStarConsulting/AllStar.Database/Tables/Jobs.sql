﻿CREATE TABLE [dbo].[Jobs]
(
	[JobId] INT NOT NULL IDENTITY(1,1),
	[Name] NVARCHAR(20) NOT NULL,
	[Description] TEXT NOT NULL,
	[HourlyWage] DECIMAL(5, 2) NOT NULL,
	[StartDate] DATE NOT NULL,
	[EndDate] DATE NULL,
	[CompletedFlag] BIT NOT NULL,
	[EmployeeName] NVARCHAR(20)  NULL,
	[UserId]                   NVARCHAR (128) NULL,

	PRIMARY KEY ([JobId]),
	FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
)
