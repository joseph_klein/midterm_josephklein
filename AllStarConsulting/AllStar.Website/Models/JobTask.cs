namespace AllStar.Website.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JobTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobTask()
        {
            Hours = new HashSet<Hour>();
        }

        [Key]
        public int TaskId { get; set; }

        [StringLength(20, ErrorMessage = "Must be less than 20 characters")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        [StringLength(500, ErrorMessage = "Must be less than 500 characters")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Estimated Hours is required")]
        [Range(0, 300, ErrorMessage = "Estimated hours must be less than 300")]
        [Display(Name = "Estimated Hours")]
        public int EstimatedHours { get; set; }

        [Display(Name = "Completion")]
        public bool CompletedFlag { get; set; }

        public int JobId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hour> Hours { get; set; }

        public virtual Job Job { get; set; }
    }
}
