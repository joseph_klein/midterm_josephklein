﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AllStar.Website.Models;
using NLog;

namespace AllStar.Website.Controllers
{
    public class HoursController : Controller
    {
        private AllStar_Database db = new AllStar_Database();
        private Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Hours/Create
        public ActionResult Create(int jobId, int taskId)
        {
            Hour hour = new Hour();
            hour.JobId = jobId;
            hour.TaskId = taskId;
            hour.Date = DateTime.Today;
      
            return View(hour);
        }

        // POST: Hours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobId,TaskId,HourId,Date,Hours")] Hour hour)
        {
            try
            { 
            if (ModelState.IsValid)
            {
                db.Hours.Add(hour);
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} created hours for {hour.Task.Name}");
                    return RedirectToAction("Edit", "Jobs", new { id = hour.JobId});
            }
            
            return View(hour);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: Hours/Edit/5
        public ActionResult Edit(int? id, int? jobId, int? taskId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hour hour = db.Hours.Find(jobId, taskId, id);
            if (hour == null)
            {
                return HttpNotFound();
            }
            return View(hour);
        }

        // POST: Hours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobId,TaskId,HourId,Date,Hours")] Hour hour)
        {
            try
            { 
            if (ModelState.IsValid)
            {
                db.Entry(hour).State = EntityState.Modified;
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} edited hours for {hour.Task.Name}");
                    return RedirectToAction("Edit", "Jobs", new { id = hour.JobId });
            }
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Name", hour.JobId);
            ViewBag.TaskId = new SelectList(db.Tasks, "TaskId", "Name", hour.TaskId);
            return View(hour);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: Hours/Delete/5
        public ActionResult Delete(int? id, int? jobId, int? taskId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hour hour = db.Hours.Find(jobId, taskId, id);
            if (hour == null)
            {
                return HttpNotFound();
            }
            return View(hour);
        }

        // POST: Hours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { 
            Hour hour = db.Hours.Find(id);
            db.Hours.Remove(hour);
            db.SaveChanges();
                logger.Info($"{User.Identity.Name} deleted hours for {hour.Task.Name}");
                return RedirectToAction("Edit", "Jobs", new { id = hour.JobId });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
