﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AllStar.Website.Models;
using NLog;

namespace AllStar.Website.Controllers
{
    public class JobTasksController : Controller
    {
        private AllStar_Database db = new AllStar_Database();
        private Logger logger = LogManager.GetCurrentClassLogger();

        // GET: JobJobTasks/Create
        public ActionResult Create(int jobId)
        {
            JobTask job = new JobTask();
                job.JobId = (int)jobId;
            
            return View(job);
        }

        // POST: JobJobTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TaskId,Name,Description,EstimatedHours,CompletedFlag,JobId")] JobTask jobTask)
        {
            try
            { 
            if (ModelState.IsValid)
            {
                db.Tasks.Add(jobTask);
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} created {jobTask.Name}");
                    return RedirectToAction("Edit", "Jobs", new { id = jobTask.JobId });
            }

            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Name", jobTask.JobId);
            return View(jobTask);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: JobJobTasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTask jobTask = db.Tasks.Find(id);
            if (jobTask == null)
            {
                return HttpNotFound();
            }
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Name", jobTask.JobId);
            return View(jobTask);
        }

        // POST: JobJobTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TaskId,Name,Description,EstimatedHours,CompletedFlag,JobId")] JobTask jobTask)
        {
            try
            { 
            if (ModelState.IsValid)
            {
                db.Entry(jobTask).State = EntityState.Modified;
                db.SaveChanges();
                    logger.Info($"{User.Identity.Name} edited {jobTask.Name}");
                    return RedirectToAction("Edit", "Jobs", new { id = jobTask.JobId });
            }
            ViewBag.JobId = new SelectList(db.Jobs, "JobId", "Name", jobTask.JobId);
            return View(jobTask);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        // GET: JobJobTasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTask jobTask = db.Tasks.Find(id);
            if (jobTask == null)
            {
                return HttpNotFound();
            }
            return View(jobTask);
        }

        // POST: JobJobTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
            JobTask jobTask = db.Tasks.Find(id);
            db.Tasks.Remove(jobTask);
            db.SaveChanges();
                logger.Info($"{User.Identity.Name} deleted {jobTask.Name}");
                return RedirectToAction("Edit", "Jobs", new { id = jobTask.JobId });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new HttpStatusCodeResult(500);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
